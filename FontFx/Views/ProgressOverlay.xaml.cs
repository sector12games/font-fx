﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressOverlay.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontFx.Views
{
    using System;
    using System.Linq;
    using System.Windows.Controls;
    using FontFx.ViewModels;

    /// <summary>
    /// Interaction logic for ProgressOverlay.xaml
    /// </summary>
    public partial class ProgressOverlay : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressOverlay"/> class.
        /// </summary>
        public ProgressOverlay()
        {
            InitializeComponent();

            // Set our animated image in the build progress view model, so that 
            // it can easily be paused or played during the build.
            BuildStatusViewModel.Instance.AnimatedImage = AnimatedImage;
        }
    }
}
