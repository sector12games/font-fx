﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressPanel.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontFx.Views
{
    using System;
    using System.Linq;
    using System.Windows.Controls;
    using System.Windows.Input;
    using FontFx.ViewModels;

    /// <summary>
    /// Interaction logic for ProgressPanel.xaml
    /// </summary>
    public partial class ProgressPanel : UserControl
    {
        private BuildStatusViewModel _viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressPanel"/> class.
        /// </summary>
        public ProgressPanel()
        {
            InitializeComponent();

            // Set our progress bars in the singleton view model. The progress bars
            // will now be able to be updated by other view models from a background 
            // worker thread.
            _viewModel = BuildStatusViewModel.Instance;
            _viewModel.ProgressBars = ProgressBars;

            // Set our data context
            DataContext = _viewModel;
        }

        /// <summary>
        /// Occurs when either the "Resume" button or the "Pause" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePlayPauseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Toggle our play/pause status. Don't start or stop the timers here. 
            // Let the worker thread do that. When we hit pause, the worker is not 
            // paused immediately. It waits until the current operation is completed. 
            _viewModel.IsPaused = !_viewModel.IsPaused;
        }

        /// <summary>
        /// Whether or not the "Pause/Resume" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePlayPauseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Only enable the pause/resume buttons if we're actually executing a build
            if (_viewModel != null && _viewModel.IsExecuting && !_viewModel.IsAborting)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        /// <summary>
        /// Occurs when the "Abort" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Tell the thread to gracefully stop itself if it is running
            _viewModel.IsAborting = true;
        }

        /// <summary>
        /// Whether or not the "Abort" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Only enable the abort button if we're actually executing a build
            if (_viewModel != null && _viewModel.IsExecuting && !_viewModel.IsAborting)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
    }
}
