﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Sector12Font.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontFx.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using FontFx.ViewModels;

    /// <summary>
    /// Interaction logic for Sector12Font.xaml
    /// </summary>
    public partial class Sector12Font : UserControl
    {
        private Sector12FontViewModel _viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sector12Font"/> class.
        /// </summary>
        public Sector12Font()
        {
            InitializeComponent();
            
            // Create our view model and set our data context
            _viewModel = new Sector12FontViewModel();
            DataContext = _viewModel;
        }

        /// <summary>
        /// Occurs when the "Build Image" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Before we do anything, verify that our hex colors are legal
            List<Color> colorPool;
            try
            {
                colorPool = HexToRgb(_viewModel.ColorPoolHex);
            }
            catch (Exception)
            {
                // Show an error message
                MessageBox.Show(
                    "There were errors parsing your color pool hex values.\n" +
                    "Please check your hex values and try again.", 
                    "Error Parsing Hex", 
                    MessageBoxButton.OK);

                // Do not allow the build to continue
                return;
            }

            // If the color parsing was successful, build the effect in a worker thread
            _viewModel.VerifiedColorPool = colorPool;
            Thread workerThread = new Thread(() => _viewModel.BuildFontFx());
            workerThread.Start();
        }

        /// <summary>
        /// Whether or not the "Play" WPF command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Don't allow the user to start a build unless all of the required
            // options are set
            if (_viewModel != null &&
                !string.IsNullOrEmpty(_viewModel.Text) &&
                !string.IsNullOrEmpty(_viewModel.ColorPoolHex))
            {
                e.CanExecute = true;
            }
        }

        /// <summary>
        /// Converts a newline delimited hex string to a list of rgb colors.
        /// </summary>
        /// <param name="hexAllColors"></param>
        /// <returns>
        /// The list of rgb colors.
        /// </returns>
        private List<Color> HexToRgb(string hexAllColors)
        {
            List<Color> result = new List<Color>();

            // Split the hex string by linebreak
            string[] hexSplit = hexAllColors.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None);

            // Attempt to convert each hex string into a Color object
            foreach (string hexSingleColor in hexSplit)
            {
                // Ignore everything after a space. This allows for comments.
                string trimmedHex = hexSingleColor;
                if (hexSingleColor.Contains(' '))
                    trimmedHex = hexSingleColor.Substring(0, hexSingleColor.IndexOf(' '));

                // Ensure there is a '#' sign at the front
                if (!trimmedHex.StartsWith("#"))
                    trimmedHex = "#" + trimmedHex;

                // Attempt to convert the hex into rgb
                result.Add((Color)ColorConverter.ConvertFromString(trimmedHex));
            }

            // Return our results
            return result;
        }
    }
}
