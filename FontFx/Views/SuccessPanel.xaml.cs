﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SuccessPanel.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontFx.Views
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using FontFx.ViewModels;

    /// <summary>
    /// Interaction logic for SuccessPanel.xaml
    /// </summary>
    public partial class SuccessPanel : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SuccessPanel"/> class.
        /// </summary>
        public SuccessPanel()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            BuildStatusViewModel.Instance.IsAdornerVisible = false;
        }
    }
}
