﻿// -----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace FontFx.Views
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using FontFx.ViewModels;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            // Create our build status data and make sure it has a reference to the
            // UIDispatcher thread. It will be needed to update UI elements from a
            // worker thread. This won't ever have to be set again, by any other
            // view models, because we have implemented this as a singleton.
            // **Make sure to call this before InitializeComponent() so that the
            // view model's UIDispatcher is set before child controls start loading
            // and accessing it.
            BuildStatusViewModel.Instance.UIDispatcher = Dispatcher;
            DataContext = BuildStatusViewModel.Instance;

            InitializeComponent();
        }

        /// <summary>
        /// Pay attention to when the application is terminating.
        /// This will happen if the user clicks the "exit" menu
        /// item, or if the user clicks the "x" in the top right.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // Code put in by default when visual studio auto-generated
            // this method.
            base.OnClosing(e);

            // Make sure our worker thread is shut down before closing the app.
            // If we don't do this, the worker thread could attempt updating the
            // gui, which will no longer exist. This will obviously cause errors.
            // We don't want to sleep here however - if the worker thread does
            // attempt an Invoke() on the UI thread, it will never get processed,
            // because we are keeping the UI locked up with a sleep. In other words,
            // our worker thread will hang because it's waiting for the UI, and 
            // the UI will hang because it's waiting for the worker. Instead,
            // just tell the worker thread that we want to abort, and cancel the
            // close operation for now. Once the worker thread has aborted and
            // is about to terminate, it will call Application.Current.Shutdown()
            // on the dispatcher thread, and this method will be called once more.
            BuildStatusViewModel.Instance.IsClosing = true;
            BuildStatusViewModel.Instance.IsAborting = true;

            // If the worker thread is not finished, don't close the window.
            // Don't sleep either. The worker thread will trigger a shutdown
            // operation once it has finished executing, and this method will
            // be called one more time.
            if (BuildStatusViewModel.Instance.IsExecuting)
                e.Cancel = true;
        }

        /// <summary>
        /// Displays the about dialog.
        /// </summary>
        private void ShowAboutDialog()
        {
            AboutDialog d = new AboutDialog();
            d.Owner = this;
            d.ShowDialog();
        }

        /// <summary>
        /// Occurs when the "Exit" menu item is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Shut down the application. This will trigger the
            // "OnClosing()" event. 
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Whether or not the command can execute.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Allow the user to exit the program at any time
            e.CanExecute = true;
        }

        /// <summary>
        /// MenuItem events bubble up, so I only have to subscribe my entire
        /// menu object to a single MenuItemClick event. All of the menu's 
        /// children will fire events that will be caught here as well.
        /// Just check the incoming event to see which menu item was the
        /// source of the event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // I implemented commands on all buttons and menu items
            // except the about menu item after I wrote this code.
            // So now, there is only one item being used here. However,
            // I decided to leave this code as is as an example for
            // future projects.
            MenuItem item = e.OriginalSource as MenuItem;
            if (item == null)
                return;

            if (item.Equals(AboutMenuItem))
                ShowAboutDialog();
        }
    }
}
