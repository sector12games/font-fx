﻿// -----------------------------------------------------------------------
// <copyright file="Sector12FontPhotoshopCalls.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace FontFx.PhotoshopLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Media;
    using Photoshop;
    using Sector12Common.PhotoshopHelpers;
    using Application = Photoshop.Application;

    /// <summary>
    /// <para>
    /// Class definition for Sector12FontPhotoshopCalls.cs
    /// </para>
    /// <para>
    /// This effect was based off of the following tutorial:
    /// http://www.tutorial9.net/tutorials/photoshop-tutorials/colorful-glowing-text-effect/
    /// </para>
    /// </summary>
    public static class Sector12FontPhotoshopCalls
    {
        private const int StartingCanvasWidth = 800;
        private const int StartingCanvasHeight = 400;

        private static Uri _actionUri;

        private static Application _ps;
        private static Document _currentDoc;

        private static int _fontSize;
        private static string _fontFamily;

        private static ArtLayer _textLayer;
        private static ArtLayer _backgroundLayer;
        private static ArtLayer _gradientFillLayer;
        private static ArtLayer _finalCharLayer;
        private static ArtLayer _finalGradientCutoutsLayer;
        private static ArtLayer _finalColorCutoutsLayer;
        private static List<ArtLayer> _charLayers;

        private static LayerSet _charLayerSet;
        private static LayerSet _gradientCutoutsLayerSet;
        private static LayerSet _colorCutoutsLayerSet;
        
        private static SolidColor _white;
        private static SolidColor _backgroundColor;

        /// <summary>
        /// Initializes photoshop for use.
        /// </summary>
        /// <param name="fontSize"></param>
        /// <param name="fontFamily"></param>
        public static void InitializePhotoshop(int fontSize, string fontFamily)
        {
            _actionUri = new Uri(
                "pack://application:,,,/Content/PhotoshopActions/" + 
                Globals.PhotoshopActionSetName + 
                ".atn");

            // Start up photoshop, if it is not already open.
            _ps = new Application();

            // Set photoshop variables
            _ps.Preferences.RulerUnits = PsUnits.psPixels;
            _ps.Preferences.TypeUnits = PsTypeUnits.psTypePixels;
            _ps.DisplayDialogs = PsDialogModes.psDisplayNoDialogs;

            // Create various colors that will be needed for building the font
            _white = new SolidColor();
            _white.RGB.Red = 255;
            _white.RGB.Green = 255;
            _white.RGB.Blue = 255;

            _fontSize = fontSize;
            _fontFamily = fontFamily;
        }

        /// <summary>
        /// <para>
        /// Creates an initial document in photoshop, with nothing but a single text
        /// element, containing all of the text the user wants to build.
        /// </para>
        /// <para>
        /// This initial document will be presented to the user, and he will be allowed
        /// to change the font size, family, etc before the actual build begins.
        /// </para>
        /// </summary>
        /// <param name="text"></param>
        /// <param name="backgroundColorHex"></param>
        public static void CreateTemplateDocument(string text, string backgroundColorHex)
        {
            // Set the background color to the color specified. Make sure to strip
            // of the hash symbol in front of the hex color, if it exists.
            _backgroundColor = new SolidColor();
            _backgroundColor.RGB.HexValue = FormatHexString(backgroundColorHex);
            _ps.BackgroundColor = _backgroundColor;

            // Create a new document
            _currentDoc = _ps.Documents.Add(
                StartingCanvasWidth,
                StartingCanvasHeight,
                InitialFill: PsDocumentFill.psTransparent);

            // Save a reference to the default layer as our background layer
            _backgroundLayer = _currentDoc.ActiveLayer;

            // Add a text layer named "text", which will contain the string we want 
            // to build
            _textLayer = _currentDoc.ArtLayers.Add();
            _textLayer.Kind = PsLayerKind.psTextLayer;
            _textLayer.Name = "text";

            // Set some default font parameters. The user can change the font settings
            // manually within photoshop when we pause in just a few seconds.
            _textLayer.TextItem.Font = _fontFamily;
            _textLayer.TextItem.Color = _white;
            _textLayer.TextItem.Size = _fontSize;

            // Set the text 
            _textLayer.TextItem.Contents = text;

            // Make sure the document is large enough to show all of our text
            _currentDoc.RevealAll();

            // Center the text within the canvas
            PhotoshopDrawingHelpers.CenterInCanvas(_textLayer, _currentDoc);

            // Finally, expand the canvas to fit all layer in our scene (if necessary)
            PhotoshopDrawingHelpers.RevealAndFill(_currentDoc, _backgroundLayer, _backgroundColor);
        }

        /// <summary>
        /// Creates the various layer sets that we will use for organization throughout
        /// the build.
        /// </summary>
        public static void CreateLayerSets()
        {
            // Create a new layer set for our basic character layers
            _charLayerSet = _currentDoc.LayerSets.Add();
            _charLayerSet.Name = "Individual Characters";

            // Create a new layer set for our gradient cutouts
            _gradientCutoutsLayerSet = _currentDoc.LayerSets.Add();
            _gradientCutoutsLayerSet.Name = "Gradient Cutouts";

            // Create a new layer set for our colored cutouts
            _colorCutoutsLayerSet = _currentDoc.LayerSets.Add();
            _colorCutoutsLayerSet.Name = "Colored Cutouts";
        }

        /// <summary>
        /// Split the string in our text layer into individual characters. Each
        /// character will be in its own layer.
        /// </summary>
        /// <param name="characterSpacing">
        /// The spacing to place between each character, before random offsets
        /// are calculated. Font kerning and spacing values will not be taken
        /// into consideration, and this value will be used in all cases.
        /// </param>
        public static void SplitTextLayer(int characterSpacing)
        {
            // Retrieve the text from the text layer. This could have been modified by
            // the user during the "pause and edit" phase.
            string text = _textLayer.TextItem.Contents.Trim();

            // Get a reference to the text layer's bounds for positioning purposes
            Rect textLayerBounds = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(_textLayer.Bounds);

            // Determine the width of a space character (' ')
            double spaceWdith = PhotoshopFontHelpers.CalculateSpaceCharacterWidth(_textLayer);

            // Create a new layer for each character in the string. Position all
            // layers side by side, starting at the left-center coordinate of the
            // original text layer. We do not need to modify the y-coordinate of
            // each new layer, as it should simply be the same as the original 
            // text layer.
            double xPos = textLayerBounds.Left;
            double yPos = textLayerBounds.Top;
            _charLayers = new List<ArtLayer>();
            foreach (char c in text)
            {
                // If the character is a space, don't add a layer, but still increment
                // our xpos value for the next character.
                if (c == ' ')
                {
                    xPos += spaceWdith + characterSpacing;
                    continue;
                }

                // Duplicate our text layer to maintain all font settings for each
                // individual character layer.
                ArtLayer charLayer = _textLayer.Duplicate();

                // Place the layer into our characters layer set
                charLayer.Move(_charLayerSet, PsElementPlacement.psPlaceInside);

                // Change the name of the layer to something more readable
                charLayer.Name = c.ToString();

                // Set the text
                charLayer.TextItem.Contents = c.ToString();

                // Rasterize the layer
                charLayer.Rasterize(PsRasterizeType.psEntireLayer);

                // Position the layer. Remember that some characters are shorter than
                // others, and their y-positions need to be bumped to account for 
                // their height. Also remember to take the specified character spacing 
                // into account.
                Rect charLayerBounds = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(charLayer.Bounds);
                PhotoshopDrawingHelpers.MoveLayerToAbsolutePosition(
                    charLayer, 
                    (int)xPos + characterSpacing, 
                    (int)(yPos + (charLayerBounds.Top - yPos)));

                // Increment the xpos for the next character
                xPos += charLayerBounds.Width + characterSpacing;

                // Add the layer to our list of characters for future use
                _charLayers.Add(charLayer);
            }

            // After completion, hide the original text layer
            _textLayer.Visible = false;

            // Once again, make sure that the document is big enough to contain
            // all of the layers we've just created.
            PhotoshopDrawingHelpers.RevealAndFill(_currentDoc, _backgroundLayer, _backgroundColor);
        }

        /// <summary>
        /// Negatively offsets each character by a random amount between the
        /// min and max values specified. In other words, it moves all of the
        /// characters closer together, and may cause them to overlap (which
        /// we want).
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public static void RandomlyOffsetCharacters(int min, int max)
        {
            // Create a new random number generator
            Random random = new Random();

            // Bump each character's x-position negatively by a random amount
            int totalBump = 0;
            foreach (ArtLayer characterLayer in _charLayers)
            {
                // Choose a random offset between min and max
                int offset = random.Next(min, max);

                // Determine the layer's current bounds
                Rect bounds = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(characterLayer.Bounds);

                // Bump the layer's x-position negatively
                PhotoshopDrawingHelpers.MoveLayerToAbsolutePosition(
                    characterLayer, 
                    (int)bounds.X - offset - totalBump, 
                    (int)bounds.Y);

                // Keep track of how much we've bumped so that we can add this
                // number to the bump of the next character as well.
                totalBump += offset;
            }
        }

        /// <summary>
        /// <para>
        /// Adds a new gradient fill layer to the document. 
        /// </para>
        /// <para>
        /// Adding a gradient fill layer is a complicated process. You have to set a 
        /// style, an angle, a scale, start and end colors, etc. Because of the 
        /// complexity, a gradient fill layer is not easily added via code. You
        /// would need to have a massive code dump from ScriptingListener, or, the
        /// cleaner method, is to execute a custom photoshop action.
        /// </para>
        /// </summary>
        public static void AddGradientFillLayer()
        {
            // Execute a pre-made action to create a vertical gradient
            PhotoshopActionHelpers.ExecuteAction(
                _ps,
                _actionUri,
                Globals.PhotoshopActionSetName,
                "CreateGradientFill");

            // Grab the active layer. This will be the gradient fill layer just 
            // created by our action
            _gradientFillLayer = _currentDoc.ActiveLayer;

            // Rasterize the layer
            _gradientFillLayer.Rasterize(PsRasterizeType.psEntireLayer);
        }

        /// <summary>
        /// Cuts out all of the individual characters from the gradient layer. This
        /// will result in all characters having a gradient.
        /// </summary>
        public static void CutOutCharacters()
        {
            List<ArtLayer> newCharLayers = new List<ArtLayer>();
            foreach (ArtLayer characterLayer in _charLayers)
            {
                // Select the layer's contents
                _currentDoc.ActiveLayer = characterLayer;
                PhotoshopSelectionHelpers.SelectActiveLayerContents(_ps);

                // Reselect our gradient layer
                _currentDoc.ActiveLayer = _gradientFillLayer;

                // Copy and paste the character from the gradient, resulting in a
                // gradient cutout of the character, in the same position as the
                // original character.
                _currentDoc.Selection.Copy();
                ArtLayer newCharacterLayer = _currentDoc.Paste();

                // Make the name of the copy be the same as the original
                newCharacterLayer.Name = characterLayer.Name;

                // Place the layer into our new cutouts layer set
                newCharacterLayer.Move(_gradientCutoutsLayerSet, PsElementPlacement.psPlaceInside);

                // Add the new layer to a new list
                newCharLayers.Add(newCharacterLayer);
            }

            // Make our character layers become the new cutout character layers
            _charLayers = newCharLayers;

            // Hide the gradient layer, we are now finished with it
            _gradientFillLayer.Visible = false;

            // Deselect everything
            _currentDoc.Selection.Deselect();
        }

        /// <summary>
        /// Chooses a random color from the list provided for each character cutout.
        /// </summary>
        /// <param name="colors"></param>
        public static void AddColorsToCutouts(List<Color> colors)
        {
            foreach (ArtLayer characterLayer in _charLayers)
            {
                // Choose a random color from our color list for each character
                Random random = new Random();
                int index = random.Next(0, colors.Count);

                // Convert the c# (wpf) color to a photoshop color
                Color csColor = colors[index];
                SolidColor psColor = new SolidColor();
                psColor.RGB.Red = csColor.R;
                psColor.RGB.Green = csColor.G;
                psColor.RGB.Blue = csColor.B;

                // Select the character layer's contents
                _currentDoc.ActiveLayer = characterLayer;
                PhotoshopSelectionHelpers.SelectActiveLayerContents(_ps);

                // Create a new layer for the color
                ArtLayer colorLayer = _currentDoc.ArtLayers.Add();

                // Make the name match the original character
                colorLayer.Name = characterLayer.Name;

                // Fill the selection with color
                _currentDoc.Selection.Fill(psColor);

                // Place the layer into our colored cutouts layer set
                colorLayer.Move(_colorCutoutsLayerSet, PsElementPlacement.psPlaceInside);
            }

            // Deselect everything
            _currentDoc.Selection.Deselect();
        }

        /// <summary>
        /// Merges all of the current layer sets into a single layer.
        /// </summary>
        public static void MergeLayerSets()
        {
            // Merge the individual characters layer set. Also hide this, as we are
            // now done with it.
            _currentDoc.ActiveLayer = _charLayerSet;
            _finalCharLayer = _charLayerSet.Merge();
            _finalCharLayer.Visible = false;

            // Merge all gradient cutout layers into one
            _currentDoc.ActiveLayer = _gradientCutoutsLayerSet;
            _finalGradientCutoutsLayer = _gradientCutoutsLayerSet.Merge();

            // Merge all color cutout layers into one
            _currentDoc.ActiveLayer = _colorCutoutsLayerSet;
            _finalColorCutoutsLayer = _colorCutoutsLayerSet.Merge();
        }

        /// <summary>
        /// <para>
        /// Creates a glow effect for the currently selected layer. This blur effect
        /// is a combination of a gaussian blur, and a blending mode, which is 
        /// complicated to pull off in code, so is executed via a custom action.
        /// </para>
        /// <para>
        /// The gaussian blur effect is performed on both the gradient cutout layer
        /// and the color cutout layer.
        /// </para>
        /// </summary>
        /// <param name="gradientCount"></param>
        /// <param name="colorCount"></param>
        public static void CreateGaussianBlurLayers(int gradientCount, int colorCount)
        {
            // Create the specified number of blur layers for gradient cutouts
            for (int i = 0; i < gradientCount; i++)
            {
                // Select the correct layer
                _currentDoc.ActiveLayer = _finalGradientCutoutsLayer;

                // Execute the action
                PhotoshopActionHelpers.ExecuteAction(
                    _ps,
                    _actionUri,
                    Globals.PhotoshopActionSetName,
                    "CreateGaussianBlurLayer");

                // Wait for the action to complete
                WaitForActionToComplete();
                
                // Name the new layer
                _currentDoc.ActiveLayer.Name = "Gradient Cutouts Blur " + i;
            }

            // Create the specified number of blur layers for color cutouts
            for (int i = 0; i < colorCount; i++)
            {
                // Select the correct layer
                _currentDoc.ActiveLayer = _finalColorCutoutsLayer;

                // Execute the action
                PhotoshopActionHelpers.ExecuteAction(
                    _ps,
                    _actionUri,
                    Globals.PhotoshopActionSetName,
                    "CreateGaussianBlurLayer");
                
                // Name the new layer
                _currentDoc.ActiveLayer.Name = "Color Cutouts Blur " + i;

                // Wait for the action to complete
                WaitForActionToComplete();
            }
        }

        /// <summary>
        /// <para>
        /// Creates the specified number of motion blur layers. A higher number
        /// gives a more dramatic effect. The motion blur is created using a
        /// custom photoshop action.
        /// </para>
        /// <para>
        /// The motion blur effect is only performed on the color cutouts layer.
        /// </para>
        /// </summary>
        /// <param name="count"></param>
        public static void CreateMotionBlurLayers(int count)
        {
            // Create the specified number of motion blurs
            for (int i = 0; i < count; i++)
            {
                // Select the correct layer
                _currentDoc.ActiveLayer = _finalColorCutoutsLayer;

                // Execute the action
                PhotoshopActionHelpers.ExecuteAction(
                    _ps,
                    _actionUri,
                    Globals.PhotoshopActionSetName,
                    "CreateMotionBlurLayer");

                // Wait for the action to complete
                WaitForActionToComplete();

                // Name the new layer
                _currentDoc.ActiveLayer.Name = "Motion Blur " + i;
            }
        }

        /// <summary>
        /// <para>
        /// Creates a reflection image using all layers previously created.
        /// The reflection is flipped vertically and positioned directly
        /// below the original image.
        /// </para>
        /// <para>
        /// The reflection that is created looks pretty good, but does not 
        /// look very realistic. In order to add some realism, this reflection
        /// will likely need to be slightly erased by hand after the build 
        /// process is completed, to create a fade.
        /// </para>
        /// </summary>
        public static void CreateReflection()
        {
            // Create a temporary layer set 
            LayerSet reflectionSet = _currentDoc.LayerSets.Add();

            // Iterate over all art layers
            foreach (ArtLayer layer in _currentDoc.ArtLayers)
            {
                // If the layer is visible, duplicate it, and add add the duplicate 
                // to our reflection set
                if (layer.Visible && layer != _backgroundLayer)
                {
                    ArtLayer duplicate = layer.Duplicate();
                    duplicate.Move(reflectionSet, PsElementPlacement.psPlaceInside);
                }
            }

            // Merge all layers in the reflection set
            _currentDoc.ActiveLayer = reflectionSet;
            ArtLayer reflectionLayer = reflectionSet.Merge();

            // Name our new layer
            reflectionLayer.Name = "Reflection (Needs Edit - Eraser)";

            // Position the reflection directly below our primary image. Do not touch
            // the layer's horizontal position. Because of blurs, the height of our
            // reflection image is probably a little bigger than our original cutouts.
            // Do not take the blur height into account, and just use the height of
            // the original cutouts for positioning.
            Rect reflectionLayerBounds = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(reflectionLayer.Bounds);
            Rect gradientLayerBounds = PhotoshopDrawingHelpers.ConvertPhotoshopBoundsToRect(_finalGradientCutoutsLayer.Bounds);
            PhotoshopDrawingHelpers.MoveLayerToAbsolutePosition(
                reflectionLayer,
                (int)reflectionLayerBounds.Left,
                (int)(reflectionLayerBounds.Top + gradientLayerBounds.Height));

            // Lessen the opacity of the relection. This can be further adjusted
            // by the user if desired. Opcaity in photoshop is a value between
            // 0 and 100.
            reflectionLayer.Opacity = 50;

            // Flip the duplicate image vetically. You can do this by resizing the
            // y-axis by -100%. All values in ArtLayer.Resize() are percentages,
            // the same as you would see if you were to perform an 
            // "Edit -> Free Transform" operation within photoshop. For more info,
            // see the following discussion (javascript only).
            // http://www.ps-scripts.com/bb/viewtopic.php?f=9&t=3660
            reflectionLayer.Resize(100, -100);
        }

        /// <summary>
        /// Add a background glow layer to the document. This layer's blend mode will
        /// be set to "overlay". By default, it won't have any effect on the overall
        /// image. In order to use it, you'll have to use a white brush on the layer to
        /// create a kind of glow.
        /// </summary>
        public static void AddBackgroundGlowLayer()
        {
            // Create and name the new glow layer
            ArtLayer glowLayer = _currentDoc.ArtLayers.Add();
            glowLayer.Name = "Background Glow (Needs Edit - White Brush)";

            // Move it to just ahead of the background layer. It needs to be
            // placed before all other layers in the document.
            glowLayer.Move(_backgroundLayer, PsElementPlacement.psPlaceBefore);

            // Set the layer's blending mode to "overlay"
            glowLayer.BlendMode = PsBlendMode.psOverlay;
        }

        /// <summary>
        /// Crops the canvas to fit the size of the contents.
        /// </summary>
        /// <param name="buffer"></param>
        public static void CropToSize(int buffer)
        {
            PhotoshopDrawingHelpers.CropToSize(
                buffer,
                _currentDoc,
                _backgroundLayer,
                _backgroundColor);
        }

        /// <summary>
        /// Formats a hexidecimal string.
        /// </summary>
        /// <param name="hex"></param>
        /// <returns>
        /// The formatted string.
        /// </returns>
        private static string FormatHexString(string hex)
        {
            // First, trim our string
            hex = hex.Trim();

            // Strip the hash symbol if it exists
            if (hex.StartsWith("#"))
            {
                int index = hex.LastIndexOf("#", StringComparison.CurrentCultureIgnoreCase);
                return hex.Substring(index + 1);
            }

            // Just return the original string if it doesn't start with a hash symbols
            return hex;
        }

        /// <summary>
        /// <para>
        /// Waits for the current photoshop action to complete.
        /// </para>
        /// </summary>
        private static void WaitForActionToComplete()
        {
            // TODO: figure out how to wait for the current action to complete
            Thread.Sleep(500);
        }
    }
}
