﻿// -----------------------------------------------------------------------
// <copyright file="BuildStatusViewModel.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace FontFx.ViewModels
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Threading;
    using Sector12Common.Controls;
    using Sector12Common.Helpers;
    using Sector12Common.ViewModels;

    /// <summary>
    /// Class definition for BuildStatusViewModel.cs
    /// </summary>
    public class BuildStatusViewModel : ViewModelBase
    {
        private static volatile BuildStatusViewModel _instance;
        private static object _syncRoot = new object();

        private DualProgressBars _progressBars;

        private DispatcherTimer _timer;
        private bool _isAdornerVisible;
        private string _buildResult;
        private string _buildResultDetails;
        private bool _isPaused;
        private int _secondsElapsed;
        private string _status;
        private bool _disableProgressBarReadabilityDelay;

        private bool _isAborting;
        private bool _isExecuting;
        private bool _isClosing;

        /// <summary>
        /// Prevents a default instance of the <see cref="BuildStatusViewModel"/> 
        /// class from being created. Required for singleton pattern.
        /// </summary>
        private BuildStatusViewModel() 
        {
            // Set up our time elapsed timer.
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(1000) };
            _timer.Tick += Timer_Tick;
        }

        /// <summary>
        /// <para>
        /// Gets an instance of ProgressPanelViewModel. Exposed as a thead-safe 
        /// singleton.
        /// </para>
        /// <para>
        /// This ViewModel is exposed as a singleton because it is required in several
        /// different unrelated areas of the code, and passing the viewmodels around
        /// as references was very difficult and confusing. In addition, there should
        /// only be one ProgressPanel, which is used by every font effect. WPF allows
        /// binding to static properties, but there seems to be a lot of issues with
        /// two-way binding, the syntax is very awkward, and I wouldn't be able to extend
        /// my Sector12Common.ViewModels.ViewModelBase (because static classes can't 
        /// extend base classes or implement interfaces), which contains all of the 
        /// OnPropertyChanged logic. Exposing this viewmodel as an instance solves all 
        /// of my problems.
        /// </para>
        /// <para>
        /// For more info on the singleton pattern, the various ways to implement it,
        /// and thread safety, check out the following discussions:
        /// http://stackoverflow.com/questions/21048536/binding-to-static-property-in-static-class-in-wpf
        /// http://stackoverflow.com/questions/17457788/c-sharp-singleton-pattern
        /// http://msdn.microsoft.com/en-us/library/ff650316.aspx
        /// </para>
        /// </summary>
        public static BuildStatusViewModel Instance
        {
            get
            {
                if (
                    _instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new BuildStatusViewModel();
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Gets or sets the UIDispatcher used to updated UI elements from a worker 
        /// thread.
        /// </summary>
        public Dispatcher UIDispatcher { get; set; }

        /// <summary>
        /// Gets or sets the animated image that will be playing while we are executing.
        /// Just for fun.
        /// </summary>
        public GifImage AnimatedImage { get; set; }

        /// <summary>
        /// Gets or sets the progress bars used for our ProgressPanel.
        /// </summary>
        public DualProgressBars ProgressBars
        {
            get
            {
                return _progressBars;
            }

            set
            {
                _progressBars = value;

                // Setup our ProgressBars for use from a worker thread. This way, we
                // can update our progress bars without having to make sure every
                // call we make is executed on the dispatcher thread.
                if (DisableProgressBarReadabilityDelay)
                    _progressBars.InitializeForUseInWorkerThread(UIDispatcher);
                else
                    _progressBars.InitializeForUseInWorkerThread(UIDispatcher, Globals.ReadingDelay);
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not our application is 
        /// executing.
        /// </para>
        /// <para>
        /// If the application is paused, IsExecuting will still return true. 
        /// If the user clicks abort, and a task is still running for a few seconds
        /// afterwards, IsExecuting will still return true until the task is finished 
        /// and the thread is dead.
        /// </para>
        /// </summary>
        public bool IsExecuting
        {
            get
            {
                return _isExecuting;
            }

            set
            {
                _isExecuting = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("IsExecuting");

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands(UIDispatcher);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the font build process 
        /// is currently paused.
        /// </summary>
        public bool IsPaused
        {
            get
            {
                return _isPaused;
            }

            set
            {
                // Dont do anything if the new value is the same as the old
                if (_isPaused == value)
                    return;

                // Update our value
                _isPaused = value;
                OnPropertyChanged("IsPaused");
                OnPropertyChanged("PlayPauseButtonText");

                // Also update our status message and start or stop our animated image
                if (IsPaused)
                {
                    Status = "Pausing... Waiting for current operation to complete...";
                    AnimatedImage.Pause();
                }
                else
                {
                    Status = "";
                    AnimatedImage.Play();
                }
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not our build is currently
        /// attempting to abort.
        /// </para>
        /// <para>
        /// Our worker thread will attempt to gracefully stop when the Abort button
        /// is clicked. This means it will finish it's current task, and stop 
        /// immediately after. If it is in the process of executing an external 
        /// photoshop script, this could take several seconds.
        /// </para>
        /// <para>
        /// While we are in the process of aborting, all buttons need to be disabled.
        /// </para>
        /// </summary>
        public bool IsAborting
        {
            get
            {
                return _isAborting;
            }

            set
            {
                _isAborting = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("IsAborting");

                // Update the status message. If we're already paused, we know the 
                // current operation is already completed, because pause waits for 
                // the current operation to complete as well.
                if (!IsPaused)
                    Status = "Aborting... Waiting for current operation to complete...";

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands(UIDispatcher);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the close/exit button have
        /// been pressed. This is needed by the worker thread. It lets the worker 
        /// thread know that it shouldn't update any UI elements on the UIDispatcher 
        /// thread anymore. If we attempt to do something like set our progress bars 
        /// to invisible while the window is in a "closing" situation, the application
        /// will hang.
        /// </summary>
        public bool IsClosing
        {
            get
            {
                return _isClosing;
            }

            set
            {
                _isClosing = value;
                OnPropertyChanged("IsClosing");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not our ProgressOverlay is 
        /// currently visible.
        /// </summary>
        public bool IsAdornerVisible
        {
            get
            {
                return _isAdornerVisible;
            }

            set
            {
                _isAdornerVisible = value;
                OnPropertyChanged("IsAdornerVisible");
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not to disable a readability 
        /// delay that I typically put in all of my WPF applications so that I have 
        /// time to read what the progress bars say before they change themselves 
        /// again. 
        /// </para>
        /// <para>
        /// The delay is only about a second or so, and does not make a significant
        /// impact when building individual zones. However, disabling the delay can 
        /// save quite a few minutes if we are doing a batch build, so I have included
        /// the option to turn it off.
        /// </para>
        /// </summary>
        public bool DisableProgressBarReadabilityDelay
        {
            get
            {
                return _disableProgressBarReadabilityDelay;
            }

            set
            {
                _disableProgressBarReadabilityDelay = value;
                OnPropertyChanged("DisableProgressBarReadabilityDelay");
            }
        }

        /// <summary>
        /// Gets or sets the total number of seconds elapsed during this build.
        /// </summary>
        public int SecondsElapsed
        {
            get
            {
                return _secondsElapsed;
            }

            set
            {
                _secondsElapsed = value;
                OnPropertyChanged("SecondsElapsed");
                OnPropertyChanged("TimerText");
                OnPropertyChanged("TimerTextLong");
            }
        }

        /// <summary>
        /// Gets the total number of seconds elapsed as a nicely formatted 
        /// timer string.
        /// </summary>
        public string TimerText
        {
            get
            {
                // Convert the seconds elapsed to a timespan for easy string formatting
                TimeSpan elapsed = new TimeSpan(_secondsElapsed * TimeSpan.TicksPerSecond);

                // Only display the hours if necessary. Always display the minutes and seconds
                if (elapsed.Hours > 0)
                    return string.Format(DateTimeHelpers.MoreThanOneHourDateFormat, elapsed);
                else
                    return string.Format(DateTimeHelpers.LessThanOneHourDateFormat, elapsed);
            }
        }

        /// <summary>
        /// Gets an even more verbose method of displaying the total seconds elapsed 
        /// to the user.
        /// </summary>
        public string TimerTextLong
        {
            get
            {
                return "Total Time Elapsed: " + TimerText;
            }
        }

        /// <summary>
        /// Gets or sets the current status of the build process. This could be a 
        /// variety of conditions - PAUSED, Pausing..., Aborting..., etc).
        /// </summary>
        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        /// <summary>
        /// Gets or sets the build result. This is displayed to the user after 
        /// completion. Should be something along the lines of "Build Successful",
        /// "Failed", or "Aborted".
        /// </summary>
        public string BuildResult
        {
            get
            {
                return _buildResult;
            }

            set
            {
                _buildResult = value;
                OnPropertyChanged("BuildResult");
            }
        }

        /// <summary>
        /// Gets or sets multi-line detail text about the build.
        /// </summary>
        public string BuildResultDetails
        {
            get
            {
                return _buildResultDetails;
            }

            set
            {
                _buildResultDetails = value;
                OnPropertyChanged("BuildResultDetails");
            }
        }

        /// <summary>
        /// <para>Gets the current play/pause button text.</para>
        /// <para>
        /// When paused, the play/pause button should display "Play". When executing,
        /// the play/pause button should display "Pause".
        /// </para>
        /// </summary>
        public string PlayPauseButtonText
        {
            get
            {
                if (IsPaused)
                    return "Resume";
                else
                    return "Pause";
            }
        }

        /// <summary>
        /// <para>
        /// Shows our progress bars, starts our timers, etc, and puts the application 
        /// into a "building" state.
        /// </para>
        /// <para>
        /// Always call this method on the UIDispatcher thead.
        /// </para>
        /// </summary>
        public void BuildStart()
        {
            // Show our progress bars
            ProgressBars.Visibility = Visibility.Visible;

            // Show our progress overlay
            IsAdornerVisible = true;

            // Our animated image can start
            AnimatedImage.Play();

            // Start our timers
            StartTimers();

            // Set our status to "executing"
            IsExecuting = true;
        }

        /// <summary>
        /// <para>
        /// Code that we always want to run after our build has completed or
        /// been aborted.
        /// </para>
        /// <para>
        /// Always call this method on the UIDispatcher thead.
        /// </para>
        /// </summary>
        public void BuildComplete()
        {
            // Hide our progress bars
            ProgressBars.Visibility = Visibility.Collapsed;

            // Update our build result label
            BuildResult = IsAborting ? "Build Aborted" : "Build Successful";

            // Stop our timers
            StopTimers();
            IsExecuting = false;

            // Our animated image can stop now
            AnimatedImage.Pause();

            // If the user tried to close the application while the worker thread
            // was executing, we will have cancelled that operation. We didn't
            // want to close the window until the worker thread was completed.
            // Now that we're done, we want to close the app, like the user 
            // previously requested.
            if (IsClosing)
                Application.Current.Shutdown();
        }

        /// <summary>
        /// Reset all variables to their default values. This will get
        /// called whenever the user clicks the build button to start a
        /// new operation.
        /// </summary>
        public void Reset()
        {
            IsAborting = false;
            IsPaused = false;
            Status = "";
            BuildResult = "";
            BuildResultDetails = "";
            SecondsElapsed = 0;

            ProgressBars.Reset(true);
        }

        /// <summary>
        /// Starts our timers.
        /// </summary>
        public void StartTimers()
        {
            if (!_timer.IsEnabled)
            {
                _timer.Start();
            }
        }

        /// <summary>
        /// Stops our timers.
        /// </summary>
        public void StopTimers()
        {
            if (_timer.IsEnabled)
            {
                _timer.Stop();
            }
        }

        /// <summary>
        /// Executes a single task and calls the NextTask() method on the progress
        /// bars control. This method will also check for any pauses or aborts before
        /// executing the given task.
        /// </summary>
        /// <param name="methodToExecute"></param>
        /// <param name="subtaskName"></param>
        /// <param name="subtaskDetails"></param>
        public void ExecuteTask(Action methodToExecute, string subtaskName = "", string subtaskDetails = "")
        {
            // Check for a pause or an abort. Wait if the application is paused,
            // and return if the user has aborted the build.
            CheckForPause();
            if (IsAborting)
                return;

            // Update the progress bars
            _progressBars.NextTask(subtaskName, subtaskDetails);

            // Execute the method
            methodToExecute();

            // For debugging
            ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Checks to see if the pause button has been pressed by the user. 
        /// If it has, it puts the worker thread to sleep and waits for the 
        /// resume button to be pressed.
        /// </summary>
        private void CheckForPause()
        {
            // If we are paused, check every second or so to see if we 
            // should resume our work.
            while (IsPaused && !IsAborting)
            {
                // If timers are already stopped, nothing happens
                StopTimers();

                // Sleep for a little bit, and then check again to see if
                // we're unpaused.
                Thread.Sleep(500);

                // Update the status after the short reading delay. This way,
                // the user always has time to see the longer "pausing... " 
                // message before switching text to "PAUSED".
                Status = "PAUSED";
            }

            // Re-start our timer
            StartTimers();

            // Remove the "PAUSED" status message
            if (Status.Equals("PAUSED"))
                Status = "";
        }

        /// <summary>
        /// Called every time our build progress timer fires.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            SecondsElapsed++;
        }
    }
}
