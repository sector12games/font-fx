﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Sector12FontViewModel.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontFx.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Media;
    using FontFx.PhotoshopLogic;
    using Sector12Common.Controls.ControlData;
    using Sector12Common.ViewModels;

    /// <summary>
    /// Class definition for Sector12FontViewModel.cs.
    /// </summary>
    public class Sector12FontViewModel : ViewModelBase
    {
        private BuildStatusViewModel _buildStatusViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sector12FontViewModel"/> class.
        /// </summary>
        public Sector12FontViewModel()
        {
            // Get a reference to our build status view model
            _buildStatusViewModel = BuildStatusViewModel.Instance;

            Text = "Sector12";
            Padding = 10;
            CreateReflection = true;
            BackgroundColorHex = "#000000";

            ColorPoolHex =
                "#ff0000 (red)" +
                Environment.NewLine +
                "#055fff (blue)" +
                Environment.NewLine +
                "#f97d03 (yellow)" +
                Environment.NewLine +
                "#0d9145 (green)";

            FontFamily = "Teacher_A";
            FontSize = 32;
            CharacterSpacing = 2;

            CutoutBlurLayerCount = 2;
            ColorBlurLayerCount = 2;
            MotionBlurLayerCount = 2;
        }

        /// <summary>
        /// Gets or sets the text of the photoshop image to build.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the font family to use for the effect. If the font 
        /// is not found, the default font to use will be determined by photoshop.
        /// </summary>
        public string FontFamily { get; set; }

        /// <summary>
        /// Gets or sets the font size to use for the effect.
        /// </summary>
        public int FontSize { get; set; }

        /// <summary>
        /// Gets or sets the spacing to place between each character, before 
        /// random offsets are calculated. Font kerning and spacing values 
        /// will not be taken into consideration, and this value will be used 
        /// in all cases.
        /// </summary>
        public int CharacterSpacing { get; set; }

        /// <summary>
        /// Gets or sets the amount of padding (px) to leave around the completed 
        /// image.
        /// </summary>
        public int Padding { get; set; }

        /// <summary>
        /// Gets or sets the hexadecimal background color of the document we will
        /// be building.
        /// </summary>
        public string BackgroundColorHex { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to generate a reflection
        /// for the image created in photoshop.
        /// </summary>
        public bool CreateReflection { get; set; }

        /// <summary>
        /// Gets or sets the list of colors to choose from when randomly selecting 
        /// colors for each character of the text.
        /// </summary>
        public string ColorPoolHex { get; set; }

        /// <summary>
        /// Gets or sets the verified and converted color pool colors. These are
        /// the results of converting our hex values to rgb.
        /// </summary>
        public List<Color> VerifiedColorPool { get; set; }

        /// <summary>
        /// Gets or sets the minimum negative offset that will be used to nudge 
        /// each character in photoshop. For each character, a random offset
        /// value is chosen between the min and max.
        /// </summary>
        public int MinOffset { get; set; }

        /// <summary>
        /// Gets or sets the maximum negative offset that will be used to nudge 
        /// each character in photoshop. For each character, a random offset
        /// value is chosen between the min and max.
        /// </summary>
        public int MaxOffset { get; set; }

        /// <summary>
        /// Gets or sets the number of cutout blur layers to use. The more cutout 
        /// layers, the more 'ghostly' and pale the appearance.
        /// </summary>
        public int CutoutBlurLayerCount { get; set; }

        /// <summary>
        /// Gets or sets the number of color blur layers to use. More color layers 
        /// will produce a bolder, more colorful effect.
        /// </summary>
        public int ColorBlurLayerCount { get; set; }

        /// <summary>
        /// Gets or sets the number of motion blur layers to use. More motion layers 
        /// will make the colored motion blur effect more pronounced.
        /// </summary>
        public int MotionBlurLayerCount { get; set; }

        /// <summary>
        /// Opens photoshop and begins the build process.
        /// </summary>
        public void BuildFontFx()
        {
            // Reset all of our build progress
            UI(() => _buildStatusViewModel.Reset());

            // Populate our progress bars with tasks
            UI(() => InitializeProgressBars());

            // Reset everything and start timers, etc.
            UI(() => _buildStatusViewModel.BuildStart());

            // Initialize Photoshop
            _buildStatusViewModel.ExecuteTask(
                () => Sector12FontPhotoshopCalls.InitializePhotoshop(FontSize, FontFamily));

            // Create a starting template for the user to modify and create various 
            // layer sets that will be used to organize the document
            _buildStatusViewModel.ExecuteTask(
                () =>
                {
                    Sector12FontPhotoshopCalls.CreateTemplateDocument(Text, BackgroundColorHex);
                    Sector12FontPhotoshopCalls.CreateLayerSets();
                });

            // Split the string into individual characters
            _buildStatusViewModel.ExecuteTask(
                () => Sector12FontPhotoshopCalls.SplitTextLayer(CharacterSpacing));

            // Bump each characte to the left by a random ammount (if greater than zero)
            _buildStatusViewModel.ExecuteTask(
                () => Sector12FontPhotoshopCalls.RandomlyOffsetCharacters(MinOffset, MaxOffset));

            // Add a gradient that fills the entire canvas and cut out each gradient 
            // character in the string into an individual layer
            _buildStatusViewModel.ExecuteTask(
                () =>
                {
                    Sector12FontPhotoshopCalls.AddGradientFillLayer();
                    Sector12FontPhotoshopCalls.CutOutCharacters();
                });

            // Choose a random color from the color pool for each character
            _buildStatusViewModel.ExecuteTask(
                () => Sector12FontPhotoshopCalls.AddColorsToCutouts(VerifiedColorPool));

            // Merge layers
            _buildStatusViewModel.ExecuteTask(
                () => Sector12FontPhotoshopCalls.MergeLayerSets());

            // Create our cutout and color blur layers using a gaussian blur filter
            _buildStatusViewModel.ExecuteTask(
                () => Sector12FontPhotoshopCalls.CreateGaussianBlurLayers(CutoutBlurLayerCount, ColorBlurLayerCount));

            // Create our motion blur layers using a motion blur filter
            _buildStatusViewModel.ExecuteTask(
                () => Sector12FontPhotoshopCalls.CreateMotionBlurLayers(MotionBlurLayerCount));

            // Create our reflection if configured
            if (CreateReflection)
                _buildStatusViewModel.ExecuteTask(() => Sector12FontPhotoshopCalls.CreateReflection());

            // Add a background glow layer that can be modified by the user after 
            // the build process has completed. Also, crop the image to fit, and 
            // optionally include some padding around all of the edges.
            _buildStatusViewModel.ExecuteTask(
                () =>
                {
                    Sector12FontPhotoshopCalls.AddBackgroundGlowLayer();
                    Sector12FontPhotoshopCalls.CropToSize(Padding);
                });

            // Stop all of our timers, update the build result
            UI(() => _buildStatusViewModel.BuildComplete());
        }

        /// <summary>
        /// Populates our progress bars with tasks.
        /// </summary>
        private void InitializeProgressBars()
        {
            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Initializing Photoshop...", "", 0));
            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Creating Template Document...", "", 0));

            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Splitting Text Layer...", "", 0));
            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Offsetting Characters...", "", 0));

            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Creating Gradient Cutouts...", "", 0));
            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Adding Colors...", "", 0));
            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Merging Layers...", "", 0));

            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Creating Gaussian Blur Layers...", "", 0));
            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Creating Motion Blur Layers...", "", 0));

            if (CreateReflection)
                _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Creating Reflection...", "", 0));

            _buildStatusViewModel.ProgressBars.Tasks.Add(new ProgressBarTask("Finalizing...", "", 0));
        }
    }
}