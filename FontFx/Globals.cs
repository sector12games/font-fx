﻿// -----------------------------------------------------------------------
// <copyright file="Globals.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace FontFx
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for Globals.cs
    /// </summary>
    public static class Globals
    {
        /// <summary>
        /// This is the photoshop action set that we will look for our photoshop
        /// actions in when running a build. The name of the photoshop action file
        /// (.atn file) should match the name of the action set itself.
        /// </summary>
        public const string PhotoshopActionSetName = "FontFx Actions - Sector12 Logo Effect";

        /// <summary>
        /// A delay used to ensure the user gets to see graphical status 
        /// messages before they dissapear.
        /// </summary>
        public const int ReadingDelay = 1000;
    }
}
